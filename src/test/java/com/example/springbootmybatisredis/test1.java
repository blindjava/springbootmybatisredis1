package com.example.springbootmybatisredis;

import com.example.springbootmybatisredis.conf.RedissonConfig;
import com.example.springbootmybatisredis.entity.TriggerMethodEnum;
import org.apache.poi.ss.formula.functions.T;
import org.apache.tomcat.jni.Thread;
import org.junit.Test;
import org.redisson.api.RBitSet;
import org.redisson.api.RedissonClient;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.expression.EvaluationContext;
import org.springframework.expression.Expression;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.common.TemplateParserContext;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.expression.spel.support.StandardEvaluationContext;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.stream.Collectors;

/**
 * ClassName:test1
 * Package:com.example.springbootmybatisredis
 * Description:
 *
 * @date:2021/5/25 17:13
 * @author:xy
 */
public class test1 {

    @Test
    public void streamT() throws InterruptedException {
           synchronized (test1.class){
        List<Integer> numbers = Arrays.asList(3, 2, 2, 3, 7, 3, 5);
        List<Integer> squaresList = numbers.stream().map(i -> 2).collect(Collectors.toList());
        List<Object> objects = Arrays.asList();
        List<Integer> collect = numbers.stream().filter(i -> i > 5).collect(Collectors.toList());
      //  collect.stream().forEach(System.out::println);
       // Thread.sleep(1000);
               System.out.println("1");
           }
    }



     @Test
     public void supplyAsyncTest() {
        //  runAsync方法：以runnable函数式接口类型为参数，所以CompletableFuture的计算结果为空。
        //    suppleAsync方法：是以Supplier<U>函数式接口类型为参数，所以计算结果类型为U。
        System.out.println("CompletableFuture supplyAsync=================");
        CompletableFuture<String> future = CompletableFuture.supplyAsync(() -> "Hello");
    	/*try {
		Thread.sleep(2000);
	 } catch (InterruptedException e1) {
		e1.printStackTrace();
	  }*/
        //  future.complete("world");
        //  future.complete("hello!!!!!!!");*/
        try {
            System.out.println(future.get());
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

    }

    @Test
  public   void piii() throws IOException {
        //测试SpringEL解析器
        String template = "#{#user}，早上好#{#df}";//设置文字模板,其中#{}表示表达式的起止，#user是表达式字符串，表示引用一个变量。
        ExpressionParser paser = new SpelExpressionParser();//创建表达式解析器

        //通过evaluationContext.setVariable可以在上下文中设定变量。
        EvaluationContext context = new StandardEvaluationContext();
        context.setVariable("user","黎明9");
        context.setVariable("user4","黎d明");
        //解析表达式，如果表达式是一个模板表达式，需要为解析传入模板解析器上下文。
        Expression expression = paser.parseExpression(template,new TemplateParserContext());

        //使用Expression.getValue()获取表达式的值，这里传入了Evalution上下文，第二个参数是类型参数，表示返回值的类型。
        System.out.println(expression.getValue(context,String.class));
    }

    }