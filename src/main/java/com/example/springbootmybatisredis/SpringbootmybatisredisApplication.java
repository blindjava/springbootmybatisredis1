package com.example.springbootmybatisredis;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.scheduling.annotation.EnableAsync;


@SpringBootApplication
@MapperScan(basePackages = "com.example.springbootmybatisredis.mapper")
@EnableCaching
@EnableAsync
public class SpringbootmybatisredisApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootmybatisredisApplication.class, args);
    }

}
