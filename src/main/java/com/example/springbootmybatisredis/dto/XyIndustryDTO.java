package com.example.springbootmybatisredis.dto;

import lombok.Data;

/**
 * ClassName:xyindustryDTO
 * Package:com.example.springbootmybatisredis.dto
 * Description:
 *
 * @date:2021/5/20 16:50
 * @author:xy
 */
@Data
public class XyIndustryDTO {

    private String industryAttr;

    private Long pid;

    private String name;
}
