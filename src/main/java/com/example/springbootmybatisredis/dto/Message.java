package com.example.springbootmybatisredis.dto;

/**
 * ClassName:message
 * Package:com.example.springbootmybatisredis.dto
 * Description:
 *
 * @date:2021/7/19 18:30
 * @author:xy
 */
import lombok.Data;
@Data
public class Message {
    String message;
    String userId;
}
