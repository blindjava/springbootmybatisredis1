package com.example.springbootmybatisredis.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.springbootmybatisredis.entity.XyIndustryName;

/**
*
*/
public interface XyIndustryNameService extends IService<XyIndustryName> {

}
