package com.example.springbootmybatisredis.service;

import com.example.springbootmybatisredis.entity.XyIndustry;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author xiyang
 * @since 2021-05-18
 */
public interface IXyIndustryService extends IService<XyIndustry> {


    public  void test();
}
