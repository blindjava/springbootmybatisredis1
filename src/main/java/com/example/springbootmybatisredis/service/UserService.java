package com.example.springbootmybatisredis.service;

import com.example.springbootmybatisredis.entity.User;

public interface UserService {
    User getInfo(Integer id);
}
