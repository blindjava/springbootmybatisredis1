package com.example.springbootmybatisredis.service.impl;

import com.example.springbootmybatisredis.entity.User;
import com.example.springbootmybatisredis.mapper.UserDaoMapper;
import com.example.springbootmybatisredis.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl   implements UserService {

    @Autowired
    private  UserDaoMapper userDaoMapper;

    @Override
    public User getInfo(Integer id) {



        return userDaoMapper.selectById(id);


    }
}
