package com.example.springbootmybatisredis.service.impl;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.springbootmybatisredis.entity.BankInfo;
import com.example.springbootmybatisredis.mapper.BankInfoMapper;
import com.example.springbootmybatisredis.service.BankInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 *
 */
@Service
public class BankInfoServiceImpl extends ServiceImpl<BankInfoMapper, BankInfo>
        implements BankInfoService {

    @Autowired
    private BankInfoMapper bankInfoMapper;


    @Override
    public List<BankInfo> fandAll() {


        return  bankInfoMapper.selectList(new QueryWrapper<BankInfo>());

    }

    @Override
    public void updateDate() {


    }

}




