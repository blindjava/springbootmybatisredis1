package com.example.springbootmybatisredis.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.springbootmybatisredis.entity.XyIndustryName;
import com.example.springbootmybatisredis.service.XyIndustryNameService;
import com.example.springbootmybatisredis.mapper.XyIndustryNameMapper;
import org.springframework.stereotype.Service;

/**
*
*/
@Service
public class XyIndustryNameServiceImpl extends ServiceImpl<XyIndustryNameMapper, XyIndustryName> implements XyIndustryNameService {

}