package com.example.springbootmybatisredis.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.example.springbootmybatisredis.dto.XyIndustryDTO;
import com.example.springbootmybatisredis.entity.XyIndustry;
import com.example.springbootmybatisredis.mapper.XyIndustryMapper;
import com.example.springbootmybatisredis.service.IXyIndustryService;
import org.springframework.beans.factory.annotation.Autowired;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author xiyang
 * @since 2021-05-18
 */
@Service
public class XyIndustryServiceImpl extends ServiceImpl<XyIndustryMapper, XyIndustry> implements IXyIndustryService {

    @Autowired
    XyIndustryMapper xyIndustryMapper;
  // @Select()
     @Override
     public  void test(){
        // XyIndustry xyIndustry = xyIndustryMapper.selectById(1);
    //     List<XyIndustry> sortByPid = xyIndustryMapper.getSortByPid();
        // System.out.println("dddddddddd"+xyIndustry);
         ArrayList<Integer> list = new ArrayList<>();
         list.add(1);
         list.add(2);
         list.add(3);
//         Collection<XyIndustry> xyIndustries = listByIds(list);
//         xyIndustries.stream().forEach(System.out::println);
         //基础
         QueryWrapper<XyIndustry> objectQueryWrapper = new QueryWrapper<>();
      //   objectQueryWrapper.lambda().eq(XyIndustry::getId,"1");
//         XyIndustry xyIndustry = xyIndustryMapper.selectOne(objectQueryWrapper);
//         XyIndustry one = getOne(objectQueryWrapper);
//         System.out.println(xyIndustry);
//         System.out.println(one);
        //分页
//      Page<XyIndustry> xyIndustryPage = new Page<XyIndustry>(2,8);
//    objectQueryWrapper.lambda().ne(XyIndustry::getId,1 ).groupBy(XyIndustry::getPid).orderByAsc(XyIndustry::getId);
      //  IPage<XyIndustry> page = page(xyIndustryPage, objectQueryWrapper);

//         Page<XyIndustry> xyIndustryIPage = xyIndustryMapper.selectPage(xyIndustryPage, objectQueryWrapper);
//         Page<TbArticle> tbArticlePage = tbArticleMapper.selectPage(page, queryWrapper);
//         return new PageResult<>(tbArticlePage.getTotal(),tbArticlePage.getRecords());
//         xyIndustryIPage.getRecords();
//        xyIndustryIPage.getTotal();
        //？？？？
         //        xyIndustryMapper.findXleftXn(1);

      //    xyIndustryMapper.findbyIds(list);

//         objectQueryWrapper.lambda().in(XyIndustry::getId,list);
//
//         List<XyIndustry> xyIndustries = xyIndustryMapper.selectList(objectQueryWrapper);

         List<XyIndustryDTO> dtoByIds = xyIndustryMapper.findDtoByIds(list);
         System.out.println(dtoByIds);

         List<Map<String, Object>> mapByIds = xyIndustryMapper.findMapByIds(list);

         XyIndustryDTO xyIndustryDTO = new XyIndustryDTO();

   //      BeanUtils.populate(Object bean, Map<String, ? extends Object> properties)

      //   BeanUtils.copyProperties(dtoByIds,xyIndustryDTO);
         System.out.println(xyIndustryDTO);
     }



}
