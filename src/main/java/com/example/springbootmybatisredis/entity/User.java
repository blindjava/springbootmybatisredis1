package com.example.springbootmybatisredis.entity;

import java.io.Serializable;
import lombok.Data;

/**
 * user
 * @author
 */
@Data
public class User implements Serializable {
    private Integer id;

    private String name;

    private Integer age;

    private Integer password;

    private Integer datetime;

    private static final long serialVersionUID = 1L;
}