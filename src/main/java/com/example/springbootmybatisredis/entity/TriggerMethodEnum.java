package com.example.springbootmybatisredis.entity;

/**
 * ClassName:dd
 * Package:com.example.springbootmybatisredis.entity
 * Description:
 *
 * @date:2021/6/24 18:06
 * @author:xy
 */
public enum TriggerMethodEnum {

    METHOD_1(1),
    METHOD_2(2),
    METHOD_3(3),
    METHOD_4(4),
    METHOD_5(5),
    METHOD_7(7),
    METHOD_6(6);

    private Integer Methodcode;
    TriggerMethodEnum(int Methodcode) {
       this.Methodcode = Methodcode;
        getMethodcode();
    }

    public Integer getMethodcode() {
        return Methodcode;
    }
}
