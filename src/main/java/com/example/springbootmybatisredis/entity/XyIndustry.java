package com.example.springbootmybatisredis.entity;

import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author xiyang
 * @since 2021-05-18
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class XyIndustry   implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer id;
    /**
     * 行业名称
     */
    private String industryName;

    /**
     * 行业编码
     */
    private String industryCode;

    /**
     * 行业属性
     */
    private String industryAttr;

    private Long pid;


}
