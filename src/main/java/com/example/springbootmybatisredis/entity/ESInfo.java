package com.example.springbootmybatisredis.entity;

import lombok.Data;

import java.util.Date;

/**
 * ClassName:ESInfo
 * Package:com.example.springbootmybatisredis.entity
 * Description:
 *
 * @date:2021/6/16 10:13
 * @author:xy
 */
@Data
public class ESInfo {
    Long   id;
    String title;
    Date  createdata;

    public ESInfo(Long id,String title, Date createdata) {
        this.id=id;
        this.title = title;
        this.createdata = createdata;
    }
}
