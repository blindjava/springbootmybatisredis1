package com.example.springbootmybatisredis.entity;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.format.DateTimeFormat;
import com.alibaba.excel.annotation.format.NumberFormat;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import jdk.nashorn.internal.ir.ReturnNode;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 
 * @TableName bank_info
 */
@TableName(value ="bank_info")
@Data
public class BankInfo implements Serializable {
    /**
     * 
     */
    @TableId(type = IdType.AUTO)
    @ExcelProperty(value = "主键", index = 0)
    private Long id;
    @ExcelProperty(value = "类型", index = 1)
    private String type;
    @ExcelProperty(value = "总数", index = 2)
    private String sum;
    @ExcelProperty(value = "名字", index = 3)
    private String name;
    @ExcelProperty(value = "描述", index = 4)
    private String descr;
    @ExcelProperty(value = "状态", index = 5)
    private Long status;
    @DateTimeFormat("yyyy年MM月dd日")
    @ExcelProperty(value = "时间", index = 6)
    private Date date;

    public static  BankInfo newInstance(Long id, String type, String sum, String name, String descr, Long status, Date date) {
        BankInfo bankInfo = new BankInfo();
         bankInfo.setDate(date);
         bankInfo.setDescr(descr);
         bankInfo.setName(name);
         bankInfo.setId(id);
         bankInfo.setStatus(status);
         bankInfo.setSum(sum);
         bankInfo.setType(type);
        return bankInfo;
    }

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        BankInfo other = (BankInfo) that;
        return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
            && (this.getType() == null ? other.getType() == null : this.getType().equals(other.getType()))
            && (this.getSum() == null ? other.getSum() == null : this.getSum().equals(other.getSum()))
            && (this.getName() == null ? other.getName() == null : this.getName().equals(other.getName()))
            && (this.getDescr() == null ? other.getDescr() == null : this.getDescr().equals(other.getDescr()))
            && (this.getStatus() == null ? other.getStatus() == null : this.getStatus().equals(other.getStatus()))
            && (this.getDate() == null ? other.getDate() == null : this.getDate().equals(other.getDate()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        result = prime * result + ((getType() == null) ? 0 : getType().hashCode());
        result = prime * result + ((getSum() == null) ? 0 : getSum().hashCode());
        result = prime * result + ((getName() == null) ? 0 : getName().hashCode());
        result = prime * result + ((getDescr() == null) ? 0 : getDescr().hashCode());
        result = prime * result + ((getStatus() == null) ? 0 : getStatus().hashCode());
        result = prime * result + ((getDate() == null) ? 0 : getDate().hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", type=").append(type);
        sb.append(", sum=").append(sum);
        sb.append(", name=").append(name);
        sb.append(", descr=").append(descr);
        sb.append(", status=").append(status);
        sb.append(", date=").append(date);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}