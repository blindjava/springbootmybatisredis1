package com.example.springbootmybatisredis.controller;


import com.example.springbootmybatisredis.conf.ThreadPoolConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;


/**
 * ClassName:login
 * Package:com.example.springbootmybatisredis.controller
 * Description:
 *
 * @date:2021/6/9 16:31
 * @author:xy
 */
@RestController
@RequestMapping()
public class loginController {

    @Resource(name = "taskExecutor")
    ThreadPoolTaskExecutor taskExecutor;

    @PostMapping("/update")
    public void update() throws InterruptedException {


        for (int i = 0; i < 100; i++) {

            taskExecutor.execute(a());


        }

    }

    Runnable a() throws InterruptedException {
        System.out.println("dddd");
        Thread.sleep(1000);
        return null;
    }

}
