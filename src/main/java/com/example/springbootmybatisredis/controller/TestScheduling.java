package com.example.springbootmybatisredis.controller;

import com.example.springbootmybatisredis.utils.DateUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * ClassName:SchedulingTest
 * Package:com.example.springbootmybatisredis.controller
 * Description:
 * scheduling 测试
 *
 * @date:2021/6/17 14:22
 * @author:xy
 */
@Component
public class TestScheduling {
    private static final Logger LOG = LoggerFactory.getLogger(TestScheduling.class);
    @Scheduled(fixedDelay=5000)
    public void myTask() throws InterruptedException {
        LOG.info("执行时间:{}", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
    }

}
//@Component
//  class TestScheduling2 {
//    private static final Logger LOG = LoggerFactory.getLogger(TestScheduling.class);
//    private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//
//    @Scheduled(fixedDelay=5000)
//    public void myTask(){
//        LOG.info("开始执行时间:{}",sdf.format(new Date()));
//        try {
//            Thread.sleep(8000);//让任务执行的耗时时间为8秒，有利于我们的观察
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
//        LOG.info("执行完毕时间:{}",sdf.format(new Date()));
//    }
//}
