package com.example.springbootmybatisredis.controller;

import com.example.springbootmybatisredis.entity.BankInfo;
import com.greenpineyu.fel.FelEngine;
import com.greenpineyu.fel.FelEngineImpl;
import com.greenpineyu.fel.context.FelContext;
import lombok.Data;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

/**
 * ClassName:FelTest
 * Package:com.example.springbootmybatisredis.controller
 * Description:
 *
 * @date:2021/6/18 14:07
 * @author:xy
 */
public class FelTest {


    //简单计算
@Test
        public void test1(){
    FelEngineImpl felEngine = new FelEngineImpl();
    FelContext ctx = felEngine.getContext();
    ctx.set("单价", 5000);
    ctx.set("数量", 12);
    ctx.set("运费", 7500);
    Object result = felEngine.eval("单价*数量+运费");
    System.out.println(result);
}

  @Test
  public  void test2 (){
      FelEngine fel = new FelEngineImpl();
      FelContext ctx = fel.getContext();
      BankInfo foo = new BankInfo();
      ctx.set("foo", foo);
      Map<String,String> m = new HashMap<String,String>();
      m.put("ElName", "fel");
      ctx.set("m",m);
       foo.setSum("112");
//调用foo.getSize()方法。
      Object result = fel.eval("foo.size");
//调用foo.isSample()方法。
      result = fel.eval("foo.Sum");

      System.out.println(result);
      System.out.println("测试提交");
//foo没有name、getName、isName方法
//foo.name会调用foo.get("name")方法。
      result = fel.eval("foo.name");

//m.ElName会调用m.get("ElName");
      result = fel.eval("m.ElName");

  }
}
