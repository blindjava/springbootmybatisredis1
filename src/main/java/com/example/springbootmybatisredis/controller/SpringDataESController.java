package com.example.springbootmybatisredis.controller;

import com.example.springbootmybatisredis.conf.ESConfig;
import org.apache.http.HttpHost;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestClientBuilder;
import org.elasticsearch.client.RestHighLevelClient;
import org.junit.Before;

import java.io.IOException;

/**
 * ClassName:SpringDataESController
 * Package:com.example.springbootmybatisredis.controller
 * Description:
 *
 * @date:2021/6/23 11:25
 * @author:xy
 */
public class SpringDataESController {


    private RestHighLevelClient Client = null;

    @Before
    public void prepare() throws IOException {

        Client = new ESConfig().elasticsearchClient();


    }






}
