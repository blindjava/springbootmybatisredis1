package com.example.springbootmybatisredis.controller;

import com.example.springbootmybatisredis.entity.User;
import com.example.springbootmybatisredis.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/")
public class UserController  {

    @Autowired
   private UserService userService;

    @GetMapping("/userinfo")
    public User getUserInfo(Integer id){
    return userService.getInfo(id);
    }

}
