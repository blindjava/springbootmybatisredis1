package com.example.springbootmybatisredis.controller;


import com.example.springbootmybatisredis.conf.RedissonConfig;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

/**
 * ClassName:CacheController
 * Package:com.example.springbootmybatisredis.controller
 * Description:
 *
 * @date:2021/6/9 16:59
 * @author:xy
 */
@RestController
@RequestMapping("/cache")
public class CacheController {

    @Cacheable(value = "save",  key = "#userName ")
    @PostMapping("/save")
    public String save(@RequestParam("username") String userName , @RequestParam("age")Integer age) throws IOException {
        RedissonClient redissonClient = new RedissonConfig().redissonClient();
        System.out.println(redissonClient.getBitSet("19"));
        System.out.println("执行方法----------------save");
        return  "age:"+age+"userName:" +userName;
    }
    public static String  dd(){
        return  "ffdsafd";
    }
    @Cacheable(value = "update",  key = " #age")
    @PostMapping("/update")
    public String update(@RequestParam("username") String userName , @RequestParam("age")Integer age){

        System.out.println("执行方法----------------update");
        return  "age:"+age+"userName:" +userName;
    }

    @CacheEvict(value = "delete",  key = " #userName")
    @PostMapping("/delete")
    public String delete(@RequestParam("username") String userName , @RequestParam("age")Integer age){

        System.out.println("执行方法----------------delete");
        return  "age:"+age+"userName:" +userName;
    }
}