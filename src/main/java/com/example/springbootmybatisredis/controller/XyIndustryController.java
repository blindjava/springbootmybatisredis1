package com.example.springbootmybatisredis.controller;

import org.springframework.validation.annotation.Validated;
import com.example.springbootmybatisredis.service.IXyIndustryService;
import com.example.springbootmybatisredis.service.impl.XyIndustryServiceImpl;
import com.mysql.cj.protocol.Message;
import com.sun.istack.internal.NotNull;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import sun.management.Agent;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author xiyang
 * @since 2021-05-18
 */
@RestController
@RequestMapping("/xy-industry")
@Validated
public class XyIndustryController {

    @Autowired
    private IXyIndustryService XyIndustryService;

    @PostMapping("test")

    private void test(@NotBlank(message = "不能为空")@RequestParam("name") String name, @Min(value = 0,message = "必须为数字") Long password){
        XyIndustryService.test();
       // System.out.println(password+"+++++"+ name);
    }


}
