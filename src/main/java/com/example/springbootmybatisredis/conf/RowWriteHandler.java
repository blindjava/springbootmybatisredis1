package com.example.springbootmybatisredis.conf;

/**
 * ClassName:dfff
 * Package:com.example.springbootmybatisredis.conf
 * Description:
 *
 * @date:2021/6/7 12:48
 * @author:xy
 */

import com.alibaba.excel.metadata.CellData;
import com.alibaba.excel.metadata.Head;
import com.alibaba.excel.write.handler.CellWriteHandler;
import com.alibaba.excel.write.metadata.holder.WriteSheetHolder;
import com.alibaba.excel.write.metadata.holder.WriteTableHolder;
import org.apache.poi.hssf.usermodel.HSSFDataFormat;
import org.apache.poi.ss.usermodel.*;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.List;

/**
 * @author jamin
 * @date 2020/7/29 15:18
 */
public class RowWriteHandler implements CellWriteHandler {
    private HashMap<Integer, List<Integer>> map;

    public RowWriteHandler(HashMap<Integer, List<Integer>> map, Short colorIndex) {
        this.map = map;
    }

    public RowWriteHandler() {
    }

    @Override
    public void beforeCellCreate(WriteSheetHolder writeSheetHolder, WriteTableHolder writeTableHolder, Row row, Head head, Integer integer, Integer integer1, Boolean aBoolean) {

    }

    @Override
    public void afterCellCreate(WriteSheetHolder writeSheetHolder, WriteTableHolder writeTableHolder, Cell cell, Head head, Integer integer, Boolean aBoolean) {

        //当前行的第i列
        // int i = cell.getColumnIndex();
        //不处理第一行
        if (0 != cell.getRowIndex()) {
            if (2 == cell.getColumnIndex()) {
                Workbook workbook = writeSheetHolder.getSheet().getWorkbook();
                CellStyle cellStyle = workbook.createCellStyle();
                DataFormat dataFormat = workbook.createDataFormat();
                cellStyle.setDataFormat(HSSFDataFormat.getBuiltinFormat("0.00"));
                cell.setCellStyle(cellStyle);
            }
        }
    }

    //加@Override会报错
    public void afterCellDataConverted(WriteSheetHolder writeSheetHolder, WriteTableHolder writeTableHolder, CellData cellData, Cell cell, Head head, Integer integer, Boolean aBoolean) {


    }

    @Override
    public void afterCellDispose(WriteSheetHolder writeSheetHolder, WriteTableHolder writeTableHolder, List<CellData> list, Cell cell, Head head, Integer integer, Boolean aBoolean) {

    }
}

