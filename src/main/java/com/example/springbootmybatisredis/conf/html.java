package com.example.springbootmybatisredis.conf;

import com.example.springbootmybatisredis.SpringbootmybatisredisApplication;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * ClassName:html
 * Package:com.example.springbootmybatisredis.conf
 * Description:
 *
 * @date:2021/7/19 18:09
 * @author:xy
 */
@SpringBootApplication
public class html implements WebMvcConfigurer {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootmybatisredisApplication.class, args);
    }

    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/").setViewName("index");
    }

}
