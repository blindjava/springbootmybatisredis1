package com.example.springbootmybatisredis.utils;


import com.baomidou.mybatisplus.core.exceptions.MybatisPlusException;
import com.baomidou.mybatisplus.core.toolkit.StringPool;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.InjectionConfig;
import com.baomidou.mybatisplus.generator.config.*;
import com.baomidou.mybatisplus.generator.config.po.TableInfo;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;
import com.baomidou.mybatisplus.generator.engine.FreemarkerTemplateEngine;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Mybatis代码生成器
 *
 * @author china
 */
public class MyBatisPlusGenerator {

	/**
	 * <p>
	 * 读取控制台内容
	 * </p>
	 */
	public static String scanner(String tip) {
		@SuppressWarnings("resource")
		Scanner scanner = new Scanner(System.in);
		StringBuilder help = new StringBuilder();
		help.append("请输入" + tip + "：");
		System.out.println(help.toString());
		if (scanner.hasNext()) {
			String ipt = scanner.next();
			if (StringUtils.isNotBlank(ipt)) {
				return ipt;
			}
		}
		throw new MybatisPlusException("请输入正确的" + tip + "！");
	}

	/**
	 * RUN THIS
	 */
	public static void main(String[] args) {
		// 代码生成器
		AutoGenerator mpg = new AutoGenerator();

		// 全局配置
		GlobalConfig gc = new GlobalConfig();
		String projectPath = System.getProperty("user.dir");
		System.out.println(projectPath);
		gc.setOutputDir(projectPath + "/src/main/java/com/example/springbootmybatisredis");
		gc.setAuthor("xiyang");
		gc.setFileOverride(true);// 是否覆盖已有文件
		gc.setEnableCache(false);// XML 二级缓存
		gc.setBaseResultMap(true);// XML ResultMap
		gc.setBaseColumnList(false);// XML columList
		gc.setOpen(false);// 是否打开输出目录
		mpg.setGlobalConfig(gc);

		// 数据源配置
		DataSourceConfig dsc = new DataSourceConfig();
		// jdbc:sqlserver://localhost:1433;DatabaseName=hotnet_ln
		// jdbc:mysql://localhost:3306/ant?useUnicode=true&useSSL=false&characterEncoding=utf8

		//dsc.setUrl("jdbc:sqlserver://localhost:1433;DatabaseName=hotnet_ln");
		//dsc.setDriverName("com.microsoft.sqlserver.jdbc.SQLServerDriver");

		dsc.setUrl("jdbc:mysql://172.24.215.10:3306/xiyang?useUnicode=true&useSSL=false&characterEncoding=utf8&serverTimezone=GMT%2B8");
		dsc.setDriverName("com.mysql.cj.jdbc.Driver");

		dsc.setUsername("root");
		dsc.setPassword("root");
		mpg.setDataSource(dsc);

		// 包配置
		PackageConfig pc = new PackageConfig();
//		pc.setModuleName(scanner("模块名"));
		pc.setParent("");
		mpg.setPackageInfo(pc);

		// 自定义配置
		InjectionConfig cfg = new InjectionConfig() {
			@Override
			public void initMap() {
				// to do nothing
			}
		};
		List<FileOutConfig> focList = new ArrayList<>();
//		String moudleName=pc.getModuleName();
		// XML文件生成位置
		focList.add(new FileOutConfig("/templates/mapper.xml.ftl") {
			@Override
			public String outputFile(TableInfo tableInfo) {
				// 自定义输入文件名称
				return projectPath + "/resources/mapper/" + /**moudleName + "/"
						+**/ tableInfo.getEntityName() + "Mapper" + StringPool.DOT_XML;
			}
		});
		cfg.setFileOutConfigList(focList);
		mpg.setCfg(cfg);
		mpg.setTemplate(new TemplateConfig().setXml(null));

		// 策略配置
		StrategyConfig strategy = new StrategyConfig();
		// 此处可以修改为您的表前缀
		strategy.setTablePrefix(new String[] { "sys_"});
		//字段前缀
		//strategy.setFieldPrefix(new String[] { "sys_"});
		// 实体类表名生成策略
		strategy.setNaming(NamingStrategy.underline_to_camel);
		// 列名生成策略
		strategy.setColumnNaming(NamingStrategy.underline_to_camel);
		// 自定义实体，公共字段
        // strategy.setSuperEntityColumns(new String[] { "test_id", "age" });
		// 自定义实体类的父类
		// strategy.setSuperEntityClass("com.baomidou.mybatisplus.samples.generator.enterprise.BaseEntity");
		strategy.setEntityLombokModel(true);
		// strategy.setSuperControllerClass("com.baomidou.mybatisplus.samples.generator.enterprise.BaseController");
		strategy.setInclude(scanner("表名"));
		// strategy.setInclude(new String[] {"sys_user","sys_role"});// 需要生成的表
		strategy.setSuperEntityColumns("id");
		strategy.setControllerMappingHyphenStyle(true);
		strategy.setTablePrefix(pc.getModuleName() + "_");
		mpg.setStrategy(strategy);
		// 选择 freemarker 引擎需要指定如下加，注意 pom 依赖必须有！
		mpg.setTemplateEngine(new FreemarkerTemplateEngine());
		mpg.execute();
	}

}
