package com.example.springbootmybatisredis.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.springbootmybatisredis.entity.BankInfo;
import org.springframework.stereotype.Component;

/**
 * @Entity com.example.easyexcel.domain.BankInfo
 */
@Component
public interface BankInfoMapper extends BaseMapper<BankInfo> {

}




