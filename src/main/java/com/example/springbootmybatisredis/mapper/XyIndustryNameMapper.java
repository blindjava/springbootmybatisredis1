package com.example.springbootmybatisredis.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.springbootmybatisredis.entity.XyIndustryName;
import org.springframework.stereotype.Component;

/**
* @Entity generator.domain.XyIndustryName
*/
@Component
public interface XyIndustryNameMapper extends BaseMapper<XyIndustryName> {


}
