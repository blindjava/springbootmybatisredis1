package com.example.springbootmybatisredis.mapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.springbootmybatisredis.dto.XyIndustryDTO;
import org.apache.ibatis.annotations.Param;
import com.example.springbootmybatisredis.entity.XyIndustry;
import org.springframework.stereotype.Component;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author xiyang
 * @since 2021-05-18
 */
@Component
public interface XyIndustryMapper extends BaseMapper<XyIndustry> {

    List<XyIndustry> getSortByPid();

    List<XyIndustry> findAllById(@Param("id") Integer id);

    int deleteByIdAndIndustryCodeIsNotNull(@Param("id") Integer id);

     List<Object> findXleftXn(@Param("id")Integer id );

     List<XyIndustry>      findbyIds(List ids);

    List<XyIndustryDTO>     findDtoByIds(List ids );
    List<Map<String, Object>>  findMapByIds (List ids);
}
