package com.example.springbootmybatisredis.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.springbootmybatisredis.entity.User;
import org.springframework.stereotype.Component;

/**
 * UserDaoMapper继承基类
 */
@Component
public interface UserDaoMapper extends  BaseMapper<User> {


}